﻿using System.Collections.Generic;
using Ax.Logic.Model.Map;
using Ax.Logic.Model.UDP;
using NUnit.Framework;

namespace Ax.Tests
{
    [TestFixture]
    internal sealed class UdpDataExtensions_Tests
    {
        [TestCaseSource(nameof(TestIsNeighborOf_Data))]
        public void TestIsNeighborOf(UdpData data1, UdpData data2, bool areNeighbors)
        {
            Assert.That(data1.IsNeighborOf(data2), Is.EqualTo(areNeighbors));
            Assert.That(data2.IsNeighborOf(data1), Is.EqualTo(areNeighbors));
        }

        public static IEnumerable<object[]> TestIsNeighborOf_Data()
        {
            yield return new object[] { new UdpData() { X = 0, Y = 7 }, new UdpData() { X = 0, Y = 8 }, true };

            yield return new object[] { new UdpData() { X = 1, Y = 8 }, new UdpData() { X = 0, Y = 7 }, false };

            yield return new object[] { new UdpData() { X = 3, Y = 2 }, new UdpData() { X = 1, Y = 2 }, false };
            yield return new object[] { new UdpData() { X = 3, Y = 2 }, new UdpData() { X = 2, Y = 2 }, true };
            yield return new object[] { new UdpData() { X = 3, Y = 2 }, new UdpData() { X = 3, Y = 0 }, false };
            yield return new object[] { new UdpData() { X = 3, Y = 2 }, new UdpData() { X = 3, Y = 1 }, true };
            yield return new object[] { new UdpData() { X = 3, Y = 2 }, new UdpData() { X = 3, Y = 3 }, true };
            yield return new object[] { new UdpData() { X = 3, Y = 2 }, new UdpData() { X = 3, Y = 8 }, false };
            yield return new object[] { new UdpData() { X = 3, Y = 2 }, new UdpData() { X = 4, Y = 2 }, true };
            yield return new object[] { new UdpData() { X = 3, Y = 2 }, new UdpData() { X = 5, Y = 2 }, false };

            yield return new object[] { new UdpData() { X = 4, Y = 4 }, new UdpData() { X = 3, Y = 3 }, false };
            yield return new object[] { new UdpData() { X = 4, Y = 4 }, new UdpData() { X = 5, Y = 3 }, true };

            yield return new object[] { new UdpData() { X = 5, Y = 2 }, new UdpData() { X = 4, Y = 1 }, false };
            yield return new object[] { new UdpData() { X = 5, Y = 2 }, new UdpData() { X = 4, Y = 3 }, false };
            yield return new object[] { new UdpData() { X = 5, Y = 2 }, new UdpData() { X = 6, Y = 1 }, true };
            yield return new object[] { new UdpData() { X = 5, Y = 2 }, new UdpData() { X = 6, Y = 3 }, true };

            yield return new object[] { new UdpData() { X = 6, Y = 1 }, new UdpData() { X = 5, Y = 0 }, true };
            yield return new object[] { new UdpData() { X = 6, Y = 1 }, new UdpData() { X = 5, Y = 2 }, true };
            yield return new object[] { new UdpData() { X = 6, Y = 1 }, new UdpData() { X = 7, Y = 0 }, false };
            yield return new object[] { new UdpData() { X = 6, Y = 1 }, new UdpData() { X = 7, Y = 3 }, false };

            yield return new object[] { new UdpData() { X = 6, Y = 3 }, new UdpData() { X = 6, Y = 1 }, false };

            yield return new object[] { new UdpData() { X = 6, Y = 4 }, new UdpData() { X = 5, Y = 3 }, false };
            yield return new object[] { new UdpData() { X = 6, Y = 4 }, new UdpData() { X = 7, Y = 3 }, true };

            yield return new object[] { new UdpData() { X = 7, Y = 0 }, new UdpData() { X = 6, Y = 0 }, true };
            yield return new object[] { new UdpData() { X = 7, Y = 0 }, new UdpData() { X = 6, Y = 1 }, false };
            yield return new object[] { new UdpData() { X = 7, Y = 0 }, new UdpData() { X = 7, Y = 7 }, false };
            yield return new object[] { new UdpData() { X = 7, Y = 0 }, new UdpData() { X = 9, Y = 1 }, false };
            yield return new object[] { new UdpData() { X = 7, Y = 0 }, new UdpData() { X = 9, Y = 2 }, false };

            yield return new object[] { new UdpData() { X = 7, Y = 4 }, new UdpData() { X = 6, Y = 5 }, false };
            yield return new object[] { new UdpData() { X = 10, Y = 7 }, new UdpData() { X = 10, Y = 8 }, true };
        }
    }
}