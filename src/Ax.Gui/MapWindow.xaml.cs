﻿using System;
using Ax.Common;
using Ax.Logic;
using Ax.Logic.Model.UDP;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using Ax.Logic.Model.Map;

namespace Ax.Gui
{
    /// <summary>
    /// Interaction logic for MapWindow.xaml
    /// </summary>
    public partial class MapWindow : Window
    {
        private readonly ArtificialIntelligence ai;
        private readonly Label[,] labelMap = new Label[Settings.Map.Rows, Settings.Map.Columns];

        public MapWindow(ArtificialIntelligence ai)
        {
            this.ai = ai;
            InitializeComponent();
            InitGrid();

            DispatcherTimer mapRefreshTimer = new DispatcherTimer();
            mapRefreshTimer.Interval = TimeSpan.FromMilliseconds(200);
            mapRefreshTimer.Tick += MapRefreshTimer_Tick;
            mapRefreshTimer.Start();
        }

        private void MapRefreshTimer_Tick(object sender, EventArgs e)
        {
            MapEntry[,] map = ai.Map.EntryMap;

            for (int y = 0; y < map.GetLength(0); y++)
            {
                for (int x = 0; x < map.GetLength(1); x++)
                {
                    Fill(y, x, map[y, x].UdpData);
                }
            }
        }

        private void InitGrid()
        {
            for (int i = 0; i < Settings.Map.Rows; i++)
                myGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            for (int i = 0; i < Settings.Map.Columns; i++)
                myGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

            for (int y = 0; y < Settings.Map.Rows; y++)
            {
                for (int x = 0; x < Settings.Map.Columns; x++)
                {
                    Border border = new Border();
                    border.BorderThickness = new Thickness(0.5);
                    border.BorderBrush = Brushes.Black;

                    Label label = new Label();
                    label.Content = $"[{x};{y}]";
                    label.FontSize = 12;
                    label.Foreground = Brushes.DarkGray;
                    label.HorizontalContentAlignment = HorizontalAlignment.Center;
                    label.VerticalContentAlignment = VerticalAlignment.Center;

                    border.Child = label;

                    Grid.SetRow(border, y);
                    Grid.SetColumn(border, x);
                    myGrid.Children.Add(border);
                    labelMap[y, x] = label;
                }
            }
        }

        public void Fill(int y, int x, UdpData data)
        {
            Dispatcher.Invoke(() =>
            {
                Label label = labelMap[y, x];
                if (data.IsEmpty || data.IsDead)
                {
                    Erase(y, x, label);
                }
                else
                {
                    string unitCount = data.Count.ToString();
                    string unitName = data.Unit.Name.ToString();
                    string totalHp = data.TotalHP.ToString();
                    string ammoLeft = data.ShootsAvailable.ToString();

                    label.Content = $"{unitCount} {unitName}\n{totalHp} HP\n{ammoLeft} ammo";
                    label.FontSize = 14;
                    label.Foreground = Brushes.Black;
                }
            });
        }

        private static void Erase(int y, int x, Label label)
        {
            label.Content = $"[{x};{y}]";
            label.FontSize = 12;
            label.Foreground = Brushes.DarkGray;
        }
    }
}