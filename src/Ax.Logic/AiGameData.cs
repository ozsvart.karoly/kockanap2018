﻿using System.Collections.Generic;
using Ax.Logic.Model.HTTP;
using Ax.Logic.Model.UDP;

namespace Ax.Logic
{
    public sealed class AiGameData
    {
        public Map Map { get; set; }
        public StanceData StanceData { get; set; }
        public UdpData CurrentUnit { get; set; }
        public List<UdpData> EnemyUnits { get; set; }

        // By strategy:
        public UdpData AttackedTarget { get;set; }
    }
}