﻿using Ax.Logic.Model.HTTP;
using Ax.Logic.Model.UDP;
using System;
using System.Collections.Generic;

namespace Ax.Logic
{
    public sealed class UnitSelector
    {
        public int MaxSquadMoney { get; }
        public int CurrentSquadMoney { get; private set; }
        public int RemainingMoney => MaxSquadMoney - CurrentSquadMoney;

        private List<string> names = new List<string>();
        private List<int> amounts = new List<int>();

        public UnitSelector(int maxSquadMoney)
        {
            MaxSquadMoney = maxSquadMoney;
        }

        public void Add(UnitName unit, int amount)
        {
            if (names.Count >= 5)
                throw new InvalidOperationException("You can only select max. 5 pairs!");

            int unitPrice = UnitPriceContainer.UnitPrices[unit];
            int totalPrice = unitPrice * amount;

            if (CurrentSquadMoney + totalPrice > MaxSquadMoney)
            {
                throw new InvalidOperationException($"Too much! You have spent {CurrentSquadMoney}," +
                                                    $" now you wanted to spend {unitPrice}*{amount}={totalPrice}," +
                                                    $" but you can only spend {MaxSquadMoney - CurrentSquadMoney} right now.");
            }

            names.Add(unit.ToString());
            amounts.Add(amount);
            CurrentSquadMoney += totalPrice;
        }

        public bool CanAdd(UnitName unit, int amount)
        {
            int unitPrice = UnitPriceContainer.UnitPrices[unit];
            int totalPrice = unitPrice * amount;

            return CurrentSquadMoney + totalPrice <= MaxSquadMoney;
        }

        public UnitSelection GetSelection()
        {
            return new UnitSelection(names, amounts);
        }
    }
}