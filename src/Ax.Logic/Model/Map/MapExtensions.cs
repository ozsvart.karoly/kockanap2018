﻿using Ax.Logic.Model.HTTP;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ax.Logic.Model.Map
{
    public static class MapExtensions
    {
        public static void ThrowIfInconsistent(this Logic.Map map)
        {
            if (map == null)
                throw new ArgumentNullException(nameof(map));

            HashSet<int> alreadyFoundIds = new HashSet<int>();

            MapEntry[,] entries = map.EntryMap;
            foreach (MapEntry mapEntry in entries)
            {
                if (mapEntry.UdpData.IsEmpty)
                    continue;

                if (alreadyFoundIds.Contains(mapEntry.UdpData.ID))
                    throw new InconsistentMapException("Duplicate entries found on the map!");

                alreadyFoundIds.Add(mapEntry.UdpData.ID);
            }
        }

        public static void DeleteDeadUnits(this Logic.Map map, StanceData stanceData)
        {
            var entries = map.EntryMap;

            HashSet<int> aliveUnitIds = new HashSet<int>(stanceData.Theirs.Concat(stanceData.Yours));
            map.CurrentAliveUnits.RemoveWhere(d => !aliveUnitIds.Contains(d.ID));

            for (int y = 0; y < entries.GetLength(0); y++)
            {
                for (int x = 0; x < entries.GetLength(1); x++)
                {
                    if (entries[y, x].UdpData.IsEmpty)
                        continue;

                    if (!aliveUnitIds.Contains(entries[y, x].UdpData.ID))
                    {
                        entries[y, x] = MapEntry.CreateEmpty(y, x);
                    }
                }
            }
        }
    }
}