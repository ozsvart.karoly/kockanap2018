﻿using System;
using Ax.Logic.Model.UDP;
using System.Collections.Generic;
using System.Linq;

namespace Ax.Logic.Model.Map
{
    public static class UdpDataListExtensions
    {
        public static UdpData GetNearest(this List<UdpData> entries, UdpData reference)
        {
            UdpData nearest = entries[0];
            double minDist = GetMathDistance(nearest, reference);

            foreach (UdpData current in entries.Skip(1))
            {
                double dist = GetMathDistance(current, reference);
                if (dist < minDist)
                {
                    nearest = current;
                    minDist = dist;
                }
            }

            return nearest;
        }

        private static double GetMathDistance(UdpData data1, UdpData data2)
        {
            int x1 = data1.X, x2 = data2.X, y1 = data1.Y, y2 = data2.Y;
            return Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
        }
    }
}