﻿using Ax.Logic.Model.UDP;
using System;

namespace Ax.Logic.Model.Map
{
    [Serializable]
    public sealed class MapEntry
    {
        /// <summary>
        /// When is this entry created?
        /// </summary>
        public DateTime CreatedAt { get; }

        /// <summary>
        /// The <see cref="UdpData"/> for the entry (tile).
        /// </summary>
        public UdpData UdpData { get; }

        public MapEntry(UdpData udpData)
        {
            CreatedAt = DateTime.Now;
            UdpData = udpData ?? throw new ArgumentNullException(nameof(udpData));
        }

        public static MapEntry CreateEmpty(int y, int x)
        {
            return new MapEntry(new UdpData()
            {
                Y = y,
                X = x
            });
        }

        public override string ToString()
        {
            if (UdpData.IsEmpty)
                return $"[{UdpData.X},{UdpData.Y}]: EMPTY";
            else
                return $"[{UdpData.X},{UdpData.Y}]: {UdpData.Count} {UdpData.Unit.Name.ToString()}s ({UdpData.ID})";
        }
    }
}