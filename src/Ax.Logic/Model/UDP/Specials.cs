﻿using System;

namespace Ax.Logic.Model.UDP
{
    [Flags]
    public enum Specials
    {
        /// <summary>
        /// It is a wide unit (2 hexagons)
        /// </summary>
        Wide = 1,

        /// <summary>
        /// Can fly (cannot be surrounded)
        /// </summary>
        Fly = 2,

        /// <summary>
        /// Ray attack (attacks 2 units)
        /// </summary>
        Ray = 4,

        /// <summary>
        /// Unit will always retaliate, not only once
        /// </summary>
        AlwaysRetal = 8,

        /// <summary>
        /// Unit will attack all surrounding units, not only one
        /// </summary>
        RoundAttack = 16,

        /// <summary>
        /// Range attack creature will not have its damage halved when attacking hand-to-hand
        /// </summary>
        NoMeleePenalty = 32,

        /// <summary>
        /// Unit has a doubled damage
        /// </summary>
        Twice = 64,

        /// <summary>
        /// Last unit's HP regenerates at the end of every round.
        /// </summary>
        Regenerates = 128,

        /// <summary>
        /// The defender unit will not retaliate
        /// </summary>
        NoRetal = 256,

        /// <summary>
        /// The attacker unit will regenerate HP from the HP inflicted on the defender
        /// </summary>
        SucksBlood = 512,

        /// <summary>
        /// ALL surrounding units are damaged next to the defender
        /// </summary>
        Blast = 1024
    }
}