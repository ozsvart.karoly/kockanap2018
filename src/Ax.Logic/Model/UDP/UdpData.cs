﻿using System;
using Ax.Common;
using Ax.Logic.MathFuck;
using Newtonsoft.Json;

namespace Ax.Logic.Model.UDP
{
    [Serializable]
    public sealed class UdpData
    {
        public bool IsEmpty => Hero == null;
        public bool IsMine => !IsEmpty && Hero.Player.Name == Settings.TeamName;
        public bool IsEnemy => !IsEmpty && Hero.Player.Name != Settings.TeamName;
        public bool IsAlive => TotalHP > 0;
        public bool IsDead => !IsAlive;

        /// <summary>
        /// Slot ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Number of units
        /// </summary>
        public int Count { get;set; }

        /// <summary>
        /// Number of units at start
        /// </summary>
        public int CountAtStart { get; set; }

        /// <summary>
        /// HP of last living creature
        /// </summary>
        public int LastHP { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public int ShootsAvailable { get; set; }

        /// <summary>
        /// Data of the hero of the unit
        /// </summary>
        public Hero Hero { get; set; }

        /// <summary>
        /// data of the current unit
        /// </summary>
        public Unit Unit { get; set; }

        /// <summary>
        /// Did the unit retaliate in this round?
        /// </summary>
        public bool RetaliatedThisRound { get; set; }

        /// <summary>
        /// total HP of all units in this slot
        /// </summary>
        public int TotalHP { get; set; }

        /// <summary>
        /// Unit.Speed + Hero.Player.Speed
        /// </summary>
        [JsonProperty(PropertyName = "Speed")]
        public int FinalSpeed { get; set; }

        /// <summary>
        /// Unit.Attack + Hero.Player.Attack
        /// </summary>
        [JsonProperty(PropertyName = "Attack")]
        public int FinalAttack { get; set; }

        /// <summary>
        /// Unit.Defense + Hero.Player.Defense
        /// </summary>
        [JsonProperty(PropertyName = "Defense")]
        public int FinalDefense { get; set; }

        private bool Equals(UdpData other)
        {
            return ID == other.ID;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is UdpData && Equals((UdpData) obj);
        }

        public override int GetHashCode()
        {
            return ID;
        }
        
        public override string ToString()
        {
            return $"X:{X}, Y:{Y}";
        }

        public Point ToPoint()
        {
            return new Point(X, Y);
        }
    }
}