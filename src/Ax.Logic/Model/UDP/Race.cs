﻿namespace Ax.Logic.Model.UDP
{
    public enum Race
    {
        Barbarian = 1,
        Knight = 2,
        Necromancer = 3,
        Neutral = 4,
        Sorceress = 5,
        Warlock = 6,
        Wizard = 7
    }
}