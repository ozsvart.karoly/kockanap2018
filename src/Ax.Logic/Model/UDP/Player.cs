﻿using System;

namespace Ax.Logic.Model.UDP
{
    [Serializable]
    public sealed class Player
    {
        public string Name { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int Speed { get; set; }

        public override string ToString() => $"{Name}, A:{Attack}, D:{Defense}, S:{Speed}";
    }
}