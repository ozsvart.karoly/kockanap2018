﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Ax.Logic.Model.HTTP
{
    public sealed class StanceData
    {
        public HashSet<int> Yours { get; private set; }
        public HashSet<int> Theirs { get; private set; }
        public int Current { get; private set; }
        public string RawString { get; private set; }

        public static StanceData Parse(NameValueCollection nameValueCollection)
        {
            string yoursDecoded = HttpUtility.UrlDecode(nameValueCollection["yours"]);
            string theirsDecoded = HttpUtility.UrlDecode(nameValueCollection["theirs"]);
            int current = int.Parse(nameValueCollection["current"]);

            List<int> yoursSplitted = Split(yoursDecoded);
            List<int> theirsSplitted = Split(theirsDecoded);

            StanceData request = new StanceData
            {
                Yours = new HashSet<int>(yoursSplitted),
                Theirs = new HashSet<int>(theirsSplitted),
                Current = current,
                RawString = $"yours={yoursDecoded}, their={theirsDecoded}, current={current}"
            };

            return request;
        }

        private static List<int> Split(string decoded)
        {
            if (string.IsNullOrEmpty(decoded))
                return new List<int>();

            return decoded.Split('|').Select(int.Parse).ToList();
        }

        public override string ToString()
        {
            return RawString;
        }
    }
}