﻿using System;
using Ax.Common;
using Ax.Logic.Model.Map;
using Ax.Logic.Model.UDP;
using System.Collections.Generic;

namespace Ax.Logic
{
    [Serializable]
    public sealed class Map
    {
        public MapEntry[,] EntryMap { get; }
        public HashSet<UdpData> CurrentAliveUnits { get; } = new HashSet<UdpData>();
        public HashSet<UdpData> AllEverSeenDatas { get; } = new HashSet<UdpData>();

        public Map()
        {
            EntryMap = new MapEntry[Settings.Map.Rows, Settings.Map.Columns];
            for (int y = 0; y < EntryMap.GetLength(0); y++)
                for (int x = 0; x < EntryMap.GetLength(1); x++)
                    EntryMap[y, x] = MapEntry.CreateEmpty(y, x);
        }

        public void Update(UdpData data)
        {
            AllEverSeenDatas.Remove(data);
            AllEverSeenDatas.Add(data);

            EntryMap.DeletePreviousUdpData(data);

            if (data.TotalHP > 0) // we don't want dead units in our vision
            {
                EntryMap[data.Y, data.X] = new MapEntry(data);
                CurrentAliveUnits.Remove(data);
                CurrentAliveUnits.Add(data);
            }
        }
    }
}