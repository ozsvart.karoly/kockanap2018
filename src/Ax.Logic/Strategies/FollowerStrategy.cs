﻿using Ax.Common;
using Ax.Logic.MathFuck;
using Ax.Logic.Model.HTTP;
using Ax.Logic.Model.Map;
using Ax.Logic.Model.UDP;
using System;
using System.Linq;

namespace Ax.Logic.Strategies
{
    public sealed class FollowerStrategy : IStrategy
    {
        public event EventHandler<string> LogMessage;

        public bool CanApply(AiGameData gameData) => gameData.CurrentUnit.Hero.IsRightSide; // TODO: remove right side restriction

        public StepRequest TryApply(AiGameData gameData)
        {
            UdpData currentUnit = gameData.CurrentUnit;
            UdpData target = gameData.EnemyUnits.Last().GetNeighbors(gameData.Map.EntryMap).GetRandomElement();

            Point point = MoveCalculator.CalculateNextMove(currentUnit.ToPoint(), target.ToPoint(), gameData.CurrentUnit.FinalSpeed);
            try
            {
                UdpData pointToMoveTo = gameData.Map.EntryMap[point.Y, point.X].UdpData;
            }
            catch (Exception e)
            {

            }


            return new StepRequest(point.X, point.Y, 0);
        }
    }
}