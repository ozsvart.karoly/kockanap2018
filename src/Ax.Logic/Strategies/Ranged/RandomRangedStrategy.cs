﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ax.Common;
using Ax.Logic.Model.HTTP;
using Ax.Logic.Model.Map;
using Ax.Logic.Model.UDP;

namespace Ax.Logic.Strategies.Ranged
{
    public class RandomRangedStrategy : IStrategy
    {
        public event EventHandler<string> LogMessage;

        public bool CanApply(AiGameData gameData)
        {
            UdpData currentUnit = gameData.CurrentUnit;
            if (!(currentUnit.Unit.IsRanged && currentUnit.ShootsAvailable > 0 ))
            {
                LogMessage?.Invoke(this, $"Unit {currentUnit.Unit.Name.ToString()} is not ranged, so {nameof(RandomRangedStrategy)} will not work..");
                return false;
            }

            List<UdpData> neighbors = currentUnit.GetNeighbors(gameData.Map.EntryMap);
            if (neighbors.Any(n => n.IsEnemy))
            {
                LogMessage?.Invoke(this, $"Ranged unit {currentUnit.Unit.Name.ToString()} has melee neighbor, so it cannot attack ranged.");
                return false;
            }
            
            return true;
        }

        public virtual StepRequest TryApply(AiGameData gameData)
        {
            UdpData randomEnemy = gameData.EnemyUnits.GetRandomElement();
            gameData.AttackedTarget = randomEnemy;

            return new StepRequest(gameData.CurrentUnit.X, gameData.CurrentUnit.Y, randomEnemy.ID);
        }
    }
}