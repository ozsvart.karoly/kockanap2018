﻿using Ax.Logic.Model.HTTP;
using Ax.Logic.Model.Map;
using Ax.Logic.Model.UDP;

namespace Ax.Logic.Strategies.Ranged
{
    public sealed class ShootNearestUnitStrategy : RandomRangedStrategy
    {
        public override StepRequest TryApply(AiGameData gameData)
        {
            UdpData nearestEnemy = gameData.EnemyUnits.GetNearest(gameData.CurrentUnit);
            gameData.AttackedTarget = nearestEnemy;

            return new StepRequest(gameData.CurrentUnit.X, gameData.CurrentUnit.Y, nearestEnemy.ID);
        }
    }
}