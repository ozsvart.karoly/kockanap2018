﻿using System;
using System.Net;
using System.Net.Sockets;

namespace Ax.Networking.UDP
{
    public sealed class AxUdpSender
    {
        private readonly IPEndPoint endPoint;

        public AxUdpSender(IPEndPoint endPoint)
        {
            this.endPoint = endPoint ?? throw new ArgumentNullException(nameof(endPoint));
        }

        public void Send(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));
            if (data.Length == 0)
                throw new ArgumentException($"{nameof(data)} cannot be empty.");

            using (UdpClient senderClient = new UdpClient())
            {
                senderClient.Send(data, data.Length, endPoint);
            }
        }
    }
}