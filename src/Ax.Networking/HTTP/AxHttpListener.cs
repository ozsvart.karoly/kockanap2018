﻿using System;
using System.Net;
using System.Text;
using System.Threading;

namespace Ax.Networking.HTTP
{
    public sealed class AxHttpListener
    {
        public event EventHandler<string> GeneralLog;

        private readonly HttpListener listener = new HttpListener();
        private readonly Func<HttpListenerRequest, string> responderMethod;

        public AxHttpListener(Func<HttpListenerRequest, string> responderMethod, params string[] prefixes)
        {
            if (prefixes == null || prefixes.Length == 0)
                throw new ArgumentException("prefixes");

            this.responderMethod = responderMethod ?? throw new ArgumentException(nameof(responderMethod));

            foreach (string s in prefixes)
                listener.Prefixes.Add(s);

            listener.Start();
        }

        public void Run()
        {
            ThreadPool.QueueUserWorkItem(o =>
            {
                RunWorkItem();
            });
        }

        private void RunWorkItem()
        {
            GeneralLog?.Invoke(this, "Webserver started...");
            try
            {
                while (listener.IsListening)
                {
                    ContinueListenning();
                }
            }
            catch (Exception e)
            {
                GeneralLog?.Invoke(this, $"Some serious exception occured when handling a request: {e}");
            }
            finally
            {
                GeneralLog?.Invoke(this, "Webserver stopped...");
            }
        }

        private void ContinueListenning()
        {
            ThreadPool.QueueUserWorkItem(c =>
            {
                HttpListenerContext context = (HttpListenerContext)c;
                try
                {
                    string response = responderMethod(context.Request);

                    byte[] responseBytes = Encoding.UTF8.GetBytes(response);

                    context.Response.ContentLength64 = responseBytes.Length;
                    context.Response.OutputStream.Write(responseBytes, 0, responseBytes.Length);
                }
                catch (Exception e)
                {
                    GeneralLog?.Invoke(this, $"Exception occured when handling a request: {e.ToString()}");
                }
                finally
                {
                    context.Response.OutputStream.Close();
                }
            }, listener.GetContext());
        }

        public void Stop()
        {
            listener.Stop();
            listener.Close();
        }
    }
}