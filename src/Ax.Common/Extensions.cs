﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Ax.Common
{
    public static class Extensions
    {
        private static Random Random = new Random();

        public static T GetRandomElement<T>(this IList<T> enumerable)
        {
            return enumerable[Random.Next(enumerable.Count)];
        }

        public static T DeepClone<T>(this T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T) formatter.Deserialize(ms);
            }
        }
    }
}