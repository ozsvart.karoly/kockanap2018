﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Ax.Common
{
    public static class FileLogger
    {
        private static readonly object udpLogLock = new object();
        private static readonly object httpLogLock = new object();
        private static readonly object aiLogLock = new object();

        public static void LogHttpToFile(string message)
        {
            Task.Run(() =>
            {
                lock (httpLogLock)
                {
                    Append(Settings.Logging.HttpLogFilePath, message);
                }
            });
        }

        public static void LogUdpToFile(string message)
        {
            Task.Run(() =>
            {
                lock (udpLogLock)
                {
                    Append(Settings.Logging.UdpLogFilePath, message);
                }
            });
        }

        public static void LogAiToFile(string message)
        {
            Task.Run(() =>
            {
                lock (aiLogLock)
                {
                    Append(Settings.Logging.AiLogFilePath, message);
                }
            });
        }


        private static void Append(string path, string message)
        {
            File.AppendAllText(path, Helper.CurrentDateTimePrefixWithMs + message + Environment.NewLine);
        }
    }
}