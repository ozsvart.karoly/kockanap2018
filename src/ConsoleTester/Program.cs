﻿using Ax.Logic;
using System;

namespace ConsoleTester
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine(UnitSelectionLogic.GetUnitSelection(20039).ToJson());
            //Console.ReadLine();

            for (int i = 100; i < int.MaxValue; i = (int)(i * 1.1))
            {
                //int value = int.Parse(Console.ReadLine());
                int value = i;

                Console.WriteLine(Environment.NewLine + Environment.NewLine + value);
                Console.WriteLine(UnitSelectionLogic.GetUnitSelection(value).ToJson());
                Console.ReadLine();
            }
        }
    }
}