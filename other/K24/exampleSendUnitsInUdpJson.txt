{
  "ID": 100, # slot ID
  "Count": 47, # number of units
  "CountAtStart": 47, # number of units at start
  "LastHP": 15, # HP of last living creature
  "X": 0, # position
  "Y": 0, # position
  "Hero": { # data of the hero of the unit
    "Player": { # player data
      "Name": "BlackIce",
      "Attack": 1,
      "Defense": 2,
      "Speed": 3
    },
    "IsReverse": false, # if True, the right player. if False, then left player
    "DamageCaused": 0, # total, during current game
    "DistanceCovered": 0, # total, during current game
    "TotalHP": 4590 # current total HP of ALL units of the hero
  },
  "Unit": { # data of the current unit
    "Filename": "Barbarian2b_OrcChieftan.png", 
    "Race": "Barbarian",
    "Rank": 3,
    "Name": "OrcChieftan",
    "HP": 15,
    "Attack": 3,
    "Defense": 4,
    "DmgMin": 3, # Minimum damage
    "DmgMax": 4, # Maximum damage
    "Speed": 3,
    "Shoots": 16, # If ranged unit, this is the ammo
    "CanShoot": true,
    "Price": 175, # PRICE!
    "Specials": 0 # List of special abilities
  },
  "RetaliatedThisRound": false, # Did the unit retaliate in this round?
  "TotalHP": 705, # total HP of all units in this slot
  "Speed": 6,
  "Attack": 4,
  "Defense": 6
}